const puppeteer = require('puppeteer');
const fs = require('fs');

// const link = 'https://www.youtube.com/playlist?list=PLqaZ-SGOwg74Ukc3VbL9Xmo0Ez3jB0pgo';
const link = 'https://www.youtube.com/playlist?list=PLqaZ-SGOwg75yUaQAWr_5EQHc3A27sf1t';

function delay(time) {
  return new Promise(function (resolve) {
    setTimeout(resolve, time);
  });
}

(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();

  await page.goto(link);
  await page.setViewport({
    width: 1200,
    height: 800,
  });

  const amountOfItems = 300;

  const amountOfScrolls = amountOfItems / 100;

  for (i = 0; i < amountOfScrolls; i++) {
    await page.evaluate(() => {
      window.scrollBy(0, window.innerHeight * 16);
    });
    await delay(1500);
  }

  await page.waitForSelector('#contents');

  await page.evaluate(() => {
    window.scrollBy(0, window.innerHeight * 16);
  });

  const linkList = await page.evaluate(() => {
    const links = document.querySelectorAll('#content a#video-title');
    const arr = [];

    links.forEach(element => {
      let currentLink = element.getAttribute('href');

      currentLink = currentLink
        .substring(0, currentLink.indexOf('&'))
        .replace('/watch?v=', '');

      arr.push(currentLink);
    });

    return arr;
  });

  console.log(`Found ${linkList.length} video IDs from the playlist`);

  fs.writeFile(
    './output/episodeIds.json',
    JSON.stringify(linkList, null, 2),
    () => {
      console.log('Created episodeIds.json file');
    }
  );

  await browser.close();
})();
