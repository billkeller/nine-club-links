const puppeteer = require('puppeteer');
const fs = require('fs');
const https = require('https');

function delay(time) {
  return new Promise(function (resolve) {
    setTimeout(resolve, time);
  });
}

const download = (url, filename) =>
  new Promise((resolve, reject) => {
    const file = fs.createWriteStream(`../public/episode-thumbs/${filename}`);

    https
      .get(url, response => {
        response.pipe(file);

        file.on('finish', () => {
          file.close(resolve(true));
        });
      })
      .on('error', error => {
        fs.unlink(filename);

        reject(error.message);
      });
  });

(async () => {
  const [fileName] = process.argv.slice(2);

  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();

  await page.goto(
    'https://www.youtube.com/playlist?list=PLqaZ-SGOwg75yUaQAWr_5EQHc3A27sf1t'
  );
  await page.setViewport({
    width: 1200,
    height: 800,
  });

  // slow scroll to ensure images have downloaded
  // for (i = 0; i < 35; i++) {
  //   await page.evaluate(() => {
  //     window.scrollBy(0, window.innerHeight);
  //   });
  //   await delay(3000);
  // }

  await page.waitForSelector('#contents');

  // const latestImage = await page.evaluate(
  //   () => document.querySelector('#contents img[src]').src
  // );

  // Find a whose title contains "#219" in it's title attribute
  const specificImage = await page.evaluate(fileNameOnPage => {
    const found = document.querySelector(
      `#contents h3[aria-label*="#${fileNameOnPage}"]`
    );
    const parent = found.closest('#container');
    const img = parent.querySelector('img');

    return img.src;
  }, fileName);

  await download(specificImage, `episode-${fileName}-live.png`);

  // tslint:disable-next-line: no-console
  console.log(' Downloaded thumbnail as: ', `episode-${fileName}-live.png`);

  // holiday 2020 is at index 8.
  // holiday 2021 is at index 60
  /*
  const holiday2021 = images.splice(8, 1);
  const holiday2020 = images.splice(59, 1);

  const reversed = images.reverse();
  console.log(
    '%c TCL ',
    'background: green; color: white',
    ' ~ reversed',
    reversed.length
  );

  const secondPart = reversed.slice(132);
  console.log(
    '%c TCL ',
    'background: green; color: white',
    ' ~ secondPart',
    secondPart
  );

  let result;

  // for (let i = 0; i < 133; i++) {
  //   let fileName = i + 1;
  //   // we don't have an episode 133, it was taken down I guess
  //   result = await download(reversed[i], `episode-${fileName}.png`);

  //   if (result === true) {
  //     console.log('Success:', reversed[i], `has been downloaded successfully.`);
  //   } else {
  //     console.log('Error:', reversed[i], 'was not downloaded.');
  //     console.error(result);
  //   }
  // }

  for (let i = 0; i < secondPart.length; i++) {
    let fileName = i + 134;
    // we don't have an episode 133, it was taken down I guess
    result = await download(secondPart[i], `episode-${fileName}.png`);

    if (result === true) {
      console.log(
        'Success:',
        secondPart[i],
        `has been downloaded successfully.`
      );
    } else {
      console.log('Error:', secondPart[i], 'was not downloaded.');
      console.error(result);
    }
  }
  */

  await browser.close();
})();
