const puppeteer = require('puppeteer');
const fs = require('fs');

// ending regex may or may not end in :
// see diff for alternate version
const regex =
  /(LINKS WE TALKED ABOUT:|LINKS WE TALKED ABOUT THIS EPISODE:|Links we talked about in this episode:|Links we talked about in this episode)([\s\S]*?)(SOCIAL MEDIA|PRODUCTS WE USE TO MAKE THE SHOW|AUDIO PODCAST|SUBSCRIBE AND LISTEN TO THE AUDIO PODCAST|Listen to The Nine Club on audio podcast|Contact Us|Time Stamps|Timestamps)/i;
const linkHrefRegex = /<a\s+(?:[^>]*?\s+)?href=(["'])(.*?)\1/;
const linkTextRegex = /.+?(?=:)/i;
const lines = /.+?(?=\n)/;
const redirectRegex = /q=([^'|"]*)/i;
const digit = /#\d+/;

(async () => {
  const getExistingEpisodes = async () => {
    return new Promise((resolve, reject) => {
      fs.readFile('./output/data.json', 'utf-8', async (err, data) => {
        const existingEpisodes = JSON.parse(data).map(item => item.id);

        resolve(existingEpisodes);
      });
    });
  };

  const existing = await getExistingEpisodes();

  fs.readFile('./output/episodeIds.json', 'utf-8', async (err, data) => {
    const allEpisodes = JSON.parse(data).reverse();

    // include ids that are NOT in existing
    const episodeSubset = allEpisodes.filter(id => !existing.includes(id));
    console.log(
      'TCL ~ file: index.js ~ line 31 ~ fs.readFile ~ episodeSubset',
      episodeSubset
    );
    let episodeNumber = null;

    for (const episodeId of episodeSubset) {
      console.log(
        '\x1b[30m\x1b[43m%s\x1b[0m',
        `working on episode: ${episodeId}`
      );
      const browser = await puppeteer.launch({ headless: false });
      const page = await browser.newPage();
      const url = `https://www.youtube.com/watch?v=${episodeId}`;
      await page.goto(url);
      const title = await page.title();
      await page.waitForSelector('#description');
      await page.waitForSelector('#info-strings');

      if (digit.test(title)) {
        episodeNumber = Number(title.match(digit)[0].replace('#', ''));
      } else {
        episodeNumber = episodeNumber + 1;
      }

      // const brief = await page.evaluate((selector) => {
      //   return document.querySelector(selector).innerText;
      // }, '#description span.style-scope.yt-formatted-string');

      // console.log('\x1b[30m\x1b[43m%s\x1b[0m', 'found brief:');
      // console.log(brief);

      const date = await page.evaluate(selector => {
        return document.querySelector(selector).innerText;
      }, '#info-strings .ytd-video-primary-info-renderer + .ytd-video-primary-info-renderer');

      // const fullDescription = await page.evaluate(selector => {
      //   return document.querySelector(selector).innerHTML;
      // }, 'div#container.ytd-video-secondary-info-renderer');

      await page.click('#description-inline-expander'); // Click "more" link first
      
      const fullDescription = await page.evaluate(selector => {
        return document.querySelector(selector).innerHTML;
      }, '#description-inline-expander');

      // Added this for episode 166: 'div#description .ytd-video-secondary-info-renderer'
      // Added for episode 206'div#container.ytd-video-secondary-info-renderer'
      console.log('\x1b[30m\x1b[43m%s\x1b[0m', 'found fullDescription:');
      console.log(fullDescription);

      const episode = {
        date,
        episode: `live-${episodeNumber}`,
        id: episodeId,
        links: [],
        title: title.replace('- YouTube', '').trim(),
        url,
      };

      try {
        const matches = fullDescription.match(regex);
        // console.log('\x1b[30m\x1b[43m%s\x1b[0m', ' ~ matches', matches);
        const found = matches[2].trim();
        const rawLinks = found.split('\n');
  
        rawLinks.forEach(linkItem => {
          if (linkItem) {
            let text = linkItem.match(linkTextRegex)
              ? linkItem.match(linkTextRegex)[0].trim()
              : '';
            let href = linkItem.match(linkHrefRegex)
              ? linkItem.match(linkHrefRegex)[2].trim()
              : '';
  
            if (text.toLowerCase() === 'switchflip switch manny') {
              href = 'https://www.youtube.com/watch?v=EejsdKHgwsU';
            } else if (href.startsWith('/watch')) {
              href = `https://www.youtube.com${href}`;
            } else if (href.startsWith('https://www.youtube.com/redirect?')) {
              const actual = href.match(redirectRegex)[1];
              href = actual
                .replace('%3A', ':')
                .replace(/%2F/g, '/')
                .replace('%3F', '?')
                .replace(/%3D/g, '=');
            }
  
            if (!text && !href) {
              text = linkItem;
              href = null;
            }
  
            episode.links.push({
              href: href?.replace('&amp;t=0s', '') ?? '',
              text,
            });
          }
        });
        
      } catch (error) {
        console.log('error: error getting rawlinks ', err)
        console.log('error: episodeNumber ', episodeNumber)
        console.log('error: episodeId ', episodeId)
      }


      console.log('\x1b[30m\x1b[43m%s\x1b[0m', 'created episode:');
      console.log(episode);
      console.log('EPISODE NUMBER: ', episodeNumber);

      fs.readFile('./output/data.json', 'utf8', (err, data) => {
        if (err) {
          console.log('error: ', err);
        } else {
          const obj = JSON.parse(data);
          obj.push(episode);
          fs.writeFile(
            './output/data.json',
            JSON.stringify(obj, null, 2),
            () => {
              console.log('-----------------------------------------');
              console.log(
                '\x1b[30m\x1b[42m%s\x1b[0m',
                'wrote to output/data.json'
              );
              console.log('');
            }
          );
        }
      });

      await browser.close();
    }
  });
})();
